package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fabrica.Fabrica;
import fabrica.FabricaFormato;
import formatos.CSV;

public class TestCSV {

	Fabrica fabrica;
	
	@Before
	public void setUp() throws Exception {
		fabrica = new FabricaFormato();
	}

	@Test
	public void testCrearFormato() {
		assertTrue(fabrica.crearFormato("CSV") instanceof CSV);
	}

}
