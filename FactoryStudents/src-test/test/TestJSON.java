package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fabrica.Fabrica;
import fabrica.FabricaFormato;
import formatos.JSON;

public class TestJSON {
	
	Fabrica fabrica;

	@Before
	public void setUp() throws Exception {
		fabrica = new FabricaFormato();
	}

	@Test
	public void testCrearFormato() {
		assertTrue(fabrica.crearFormato("JSON") instanceof JSON);
	}

}
