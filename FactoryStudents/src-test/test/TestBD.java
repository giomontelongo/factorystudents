package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fabrica.BaseDatos;

public class TestBD {
	
	BaseDatos bd;

	@Before
	public void setUp() throws Exception {
		
		bd = new BaseDatos();
		bd.construirBDatos();
	}

	@Test
	public void testGetEncabezado() {
		assertTrue(bd.getEncabezado().length > 0);
	}

	@Test
	public void testGetBDatos() {
		assertTrue(bd.getBDatos().size() > 0);
	}

}
