package app;

import fabrica.Fabrica;
import fabrica.FabricaFormato;
import fabrica.BaseDatos;

public class Simulador {

	public static void main(String[] args) {
		
		Fabrica fabrica1 = new FabricaFormato();
		BaseDatos bd = new BaseDatos();
		bd.construirBDatos();
		
		fabrica1.formato("JSON", bd);
		fabrica1.formato("CSV", bd);
		fabrica1.formato("TXT", bd);
		fabrica1.formato("XML", bd);
	}

}
