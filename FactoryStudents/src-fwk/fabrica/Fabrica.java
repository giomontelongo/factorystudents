package fabrica;

import fabrica.BaseDatos;
import formatos.Formato;

public abstract class Fabrica {
	
	public void formato(String tipo, BaseDatos bd){
		Formato f = crearFormato(tipo);
		f.tipoFormato(bd);
	}
	
	public abstract Formato crearFormato(String tipo);
}
