package fabrica;

import formatos.Formato;
import formatos.TXT;
import formatos.CSV;
import formatos.XML;
import formatos.JSON;

public class FabricaFormato extends Fabrica {

	@Override
	public Formato crearFormato(String tipo) {
		if(tipo.equals("TXT")){
			return new TXT();
		}
		if(tipo.equals("CSV")){
			return new CSV();
		}
		if(tipo.equals("XML")){
			return new XML();
		}
		else{
			return new JSON();
		}
	}
}