package fabrica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class BaseDatos {
	
	ArrayList<String> datos;
	String [] encabezado = {"No. De Control","Nombre","Apellido Paterno","Apellido Materno","Edad","Sexo","Carrera","Semestre"};
	
	public void construirBDatos() {
		
		File archivo = null;
		FileReader fr = null;
		
		datos = new ArrayList<String>();
		
		try {
			archivo = new File ("/Users/giovannimanjarrezmontelongo/git/FactoryStudents/FactoryStudents/file/archivo.txt");
			String linea;
			fr = new FileReader (archivo);
			BufferedReader br = new BufferedReader(fr);
			
			while((linea = br.readLine())!=null){
				datos.add(linea);
			}
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	
	public String[] getEncabezado(){
		return encabezado;
	}
	
	public ArrayList<String> getBDatos(){
		return datos;
	}
}
