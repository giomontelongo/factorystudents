package formatos;

import java.io.File;
import java.io.FileWriter;

import fabrica.BaseDatos;

public class CSV extends Formato {

	@Override
	public void tipoFormato(BaseDatos baseDatos) {
		System.out.println("Formato CSV");
		
		try {
			File archivo=new File("/Users/giovannimanjarrezmontelongo/git/FactoryStudents/FactoryStudents/file/baseDatos.csv");
			FileWriter escribir=new FileWriter(archivo,true);
			
			//Encabezado
			for(String encabezado : baseDatos.getEncabezado() ){
				escribir.append(encabezado);
				escribir.append(",");
	            escribir.flush();
			}
			escribir.append("\n");
			
			for(int i = 0; i < baseDatos.getBDatos().size();i++ ){
				String line = baseDatos.getBDatos() .get(i);
				String csv = line.replace("-", ",");
	            escribir.append(csv);
	            escribir.append("\n");
	            escribir.flush();
			}
			
			escribir.close();
		} catch(Exception e){
			System.out.println("Error al escribir");
		}
	}

}
