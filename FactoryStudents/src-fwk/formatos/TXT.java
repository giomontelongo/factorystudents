package formatos;

import java.io.File;
import java.io.FileWriter;

import fabrica.BaseDatos;

public class TXT extends Formato {

	@Override
	public void tipoFormato(BaseDatos baseDatos) {
		System.out.println("Formato TXT");
		
		try {
			File archivo=new File("/Users/giovannimanjarrezmontelongo/git/FactoryStudents/FactoryStudents/file/baseDatos.txt");
			FileWriter escribir=new FileWriter(archivo,true);
			
			//Encabezado
			for(String encabezado : baseDatos.getEncabezado()){
				escribir.write(encabezado);
				escribir.write("|");
			}
			
			escribir.write("\n");
			
			for(int i = 0; i < baseDatos.getBDatos().size();i++ ){
				String line = baseDatos.getBDatos().get(i);
				String numeroControl = null;
		        String nombre = null;
		        String apellidoPaterno = null;
		        String apellidoMaterno = null;
		        String edad = null;
		        String sexo = null;
		        String carrera = null;
		        String semestre = null;
		        
		    	String [] datos = line.split("-");
	
		        numeroControl = datos[0];
	    		nombre = datos[1];
	    		apellidoPaterno = datos[2];
	    		apellidoMaterno = datos[3];
	    		edad = datos[4];
	    		sexo = datos[5];
	    		carrera = datos[6];
	    		semestre = datos[7];

				escribir.write(numeroControl);
				escribir.write("|");
				escribir.write(nombre);
				escribir.write("|");
				escribir.write(apellidoPaterno);
				escribir.write("|");
				escribir.write(apellidoMaterno);
				escribir.write("|");
				escribir.write(edad);
				escribir.write("|");
				escribir.write(sexo);
				escribir.write("|");
				escribir.write(carrera);
				escribir.write("|");
				escribir.write(semestre);
				escribir.write("|");
				escribir.write("\n");
			}
			
			escribir.close();

		} catch(Exception e){
			System.out.println("Error al escribir");
		}
	}

}
