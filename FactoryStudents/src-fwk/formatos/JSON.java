package formatos;

import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import fabrica.BaseDatos;

public class JSON extends Formato {

	@Override
	public void tipoFormato(BaseDatos baseDatos) {
		System.out.println("Formato JSON");
		
		try {
			FileWriter file = new FileWriter("/Users/giovannimanjarrezmontelongo/git/FactoryStudents/FactoryStudents/file/baseDatos.json");
			
			
			
			
			for(int i = 0; i < baseDatos.getBDatos().size();i++ ){
				String line = baseDatos.getBDatos().get(i);
				String numeroControl = null;
		        String nombre = null;
		        String apellidoPaterno = null;
		        String apellidoMaterno = null;
		        String edad = null;
		        String sexo = null;
		        String carrera = null;
		        String semestre = null;
		        
		    	String[] datos = line.split("-");
		  
		    	numeroControl = datos[0];
				nombre = datos[1];
				apellidoPaterno = datos[2];
				apellidoMaterno = datos[3];
				edad = datos[4];
				sexo = datos[5];
				carrera = datos[6];
				semestre = datos[7];
    	
				String lineaJson = 
						" [{numeroControl: " + numeroControl + ", Nombre: " + nombre + 
						", Apellido Paterno: " + apellidoPaterno + ", Apellido Mateno:" + apellidoMaterno +
						", Edad: " + edad + ", Sexo: "+ sexo + ", Carrera: " + carrera +
						", Semestre: "+ semestre + "}]";
    	
				
				JSONObject obj = new JSONObject();		
				obj.put("Alumnos["+i+"]", lineaJson);
		
				file.write(obj.toString());
				file.flush();
			}
			file.close();
		} catch (IOException e) {
			System.out.println("Error: " + e);
		} catch (JSONException e) {
			System.out.println("Error JSON: " + e);
		}
	}

}
