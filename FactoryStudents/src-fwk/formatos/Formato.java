package formatos;

import fabrica.BaseDatos;

public abstract class Formato {
	
	public abstract void tipoFormato(BaseDatos baseDatos);
}
