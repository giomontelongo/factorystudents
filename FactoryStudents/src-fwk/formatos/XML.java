package formatos;

import fabrica.BaseDatos;
import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XML extends Formato {

	@Override
	public void tipoFormato(BaseDatos baseDatos) {
		System.out.println("Formato XML");
		
		XMLOutputFactory factory;
		XMLStreamWriter writer;
		
		try {
			factory = XMLOutputFactory .newInstance();
			writer = factory.createXMLStreamWriter(new FileWriter("/Users/giovannimanjarrezmontelongo/git/FactoryStudents/FactoryStudents/file/baseDatos.xml")); 
			writer.writeStartElement("Alumno");

			for(int i = 0; i < baseDatos.getBDatos().size();i++ ){
				String line = baseDatos.getBDatos().get(i);
				
				String numeroControl = null;
	            String nombre = null;
	            String apellidoPaterno = null;
	            String apellidoMaterno = null;
	            String edad = null;
	            String sexo = null;
	            String carrera = null;
	            String semestre = null;
            
	            String[] datos = line.split("-");
        	
	            numeroControl = datos[0];
	    		nombre = datos[1];
	    		apellidoPaterno = datos[2];
	    		apellidoMaterno = datos[3];
	    		edad = datos[4];
	    		sexo = datos[5];
	    		carrera = datos[6];
	    		semestre = datos[7];
        	
	            writer.writeStartElement("numeroControl");
	            writer.writeCharacters(numeroControl);
	            writer.writeEndElement();
	            writer.writeStartElement("Nombre");
	            writer.writeCharacters(nombre);
	            writer.writeEndElement();
	            writer.writeStartElement("ApellidoPaterno");
	            writer.writeCharacters(apellidoPaterno);
	            writer.writeEndElement();
	            writer.writeStartElement("ApellidoMaterno");
	            writer.writeCharacters(apellidoMaterno);
	            writer.writeEndElement();
	            writer.writeStartElement("Edad");
	            writer.writeCharacters(edad);
	            writer.writeEndElement();
	            writer.writeStartElement("Sexo");
	            writer.writeCharacters(sexo);
	            writer.writeEndElement();
	            writer.writeStartElement("Carrera");
	            writer.writeCharacters(carrera);
	            writer.writeEndElement();
	            writer.writeStartElement("Semestre");
	            writer.writeCharacters(semestre);
	            writer.writeEndElement();
			}
			
            writer.writeEndDocument();
            writer.flush();
            writer.close();
            
		} catch(Exception e){
			System.out.println("Error: " + e);
		}
	}

}
